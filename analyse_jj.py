
import pandas as pd
from datetime import datetime, timedelta
from flask import render_template, request
from Data_cleaning.clean_adh import clean_adh_data
from Data_cleaning.clean_euros_players import clean_euros_players
from Data_cleaning.clean_evo_jj import clean_evo_jj
from Data_cleaning.clean_operations import load_operations
from Data_cleaning.clean_theo import clean_theo


def is_number(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def check_date_range_jj(df, date_start_obj, date_end_obj, date_start_n_1_obj, date_end_n_1_obj, dataframe_name):
    min_date = df['date'].min()
    if date_start_obj < min_date:
        return f"Nous ne disposons des données qu'à partir du {min_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    max_date = df['date'].max()
    if date_end_obj > max_date:
        return f"Nous ne disposons que des données jusqu'au {max_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    min_date_n_1 = df['date'].min()
    if date_start_n_1_obj < min_date_n_1:
        return f"Nous ne disposons des données qu'à partir du {min_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    max_date_n_1 = df['date'].max()
    if date_end_n_1_obj > max_date_n_1:
        return f"Nous ne disposons que des données jusqu'au {max_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    return None



# @app.route('/analyse-jj', methods=['GET', 'POST'])
def analyse_jj_view():
    date_start = date_end = None
    dataframe = pd.DataFrame()
    df_filtre_euros_players = pd.DataFrame()
    df_operations = pd.DataFrame()
    filtre_evo_jj = pd.DataFrame()

    if request.method == 'POST':
        date_start = request.form.get('date-depart')
        date_end = request.form.get('date-arrivee')
        year_comparaison = request.form.get('year-comparaison')

        # Convertir l'année de comparaison en entier, ou utiliser une valeur par défaut si elle est vide
        if year_comparaison:
            year_comparaison = int(year_comparaison)
        else:
            year_comparaison = datetime.now().year - 1

        ####################### Vérifier dates  ################################
        # Vérifier que date_start et date_end ne sont pas vides
        if date_start and date_end and year_comparaison :
            ############################# Période N ############################
            # Convertir les chaînes de caractères en objets de date
            date_start_obj = datetime.strptime(date_start, '%Y-%m-%d')
            date_end_obj = datetime.strptime(date_end, '%Y-%m-%d')

            # Formater les dates dans le format souhaité (dd-mm-yyyy)
            date_start_formatted = date_start_obj.strftime('%d-%m-%Y')
            date_end_formatted = date_end_obj.strftime('%d-%m-%Y')

            print("Date de début N:", date_start_formatted)
            print("Date de fin N:", date_end_formatted)

            #################### Période N-1 date de départ ####################
            # Vérifiez si l'année précédente est une année bissextile
            def is_leap_year(year):
                return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)

            # Aller à la même date de l'année précédente, mais si le jour est \
                # 29 et que l'année précédente n'est pas bissextile, définir le \
                    # jour à 28
            prev_year_date = date_start_obj.replace(day=28) if date_start_obj.day \
                == 29 and not is_leap_year(date_start_obj.year - 1) else date_start_obj
            # prev_year_date = prev_year_date.replace(year = date_start_obj.year - 1)
            prev_year_date = prev_year_date.replace(year = year_comparaison)

            # Vérifiez le jour de la semaine pour date_start_obj et prev_year_date
            day_of_week_date_obj = date_start_obj.weekday()
            day_of_week_prev_year = prev_year_date.weekday()

            # Trouver le jour de la semaine le plus proche dans l'année précédente
            for i in range(7):
                temp_date = prev_year_date + timedelta(days=i)
                if temp_date.weekday() == day_of_week_date_obj:
                    closest_day = temp_date
                    break
                temp_date = prev_year_date - timedelta(days=i)
                if temp_date.weekday() == day_of_week_date_obj:
                    closest_day = temp_date
                    break

            # Conversion au format français
            date_start_n_1_obj = closest_day
            date_start_n_1_formatted = closest_day.strftime('%d-%m-%Y')

            #################### Période N-1 date de fin ####################
            # Vérifiez si l'année précédente est une année bissextile
            def is_leap_year(year):
                return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)

            # Aller à la même date de l'année précédente, mais si le jour est \
                # 29 et que l'année précédente n'est pas bissextile, définir le \
                    # jour à 28
            prev_year_date2 = date_end_obj.replace(day=28) if date_end_obj.day \
                == 29 and not is_leap_year(date_end_obj.year - 1) else date_end_obj
            # prev_year_date2 = prev_year_date2.replace(year = date_end_obj.year - 1)
            prev_year_date2 = prev_year_date2.replace(year=year_comparaison)


            # Vérifiez le jour de la semaine
            day_of_week_date_obj2 = date_end_obj.weekday()
            # day_of_week_prev_year2 = prev_year_date2.weekday()

            # Trouver le jour de la semaine le plus proche dans l'année précédente
            for i in range(7):
                temp_date2 = prev_year_date2 + timedelta(days=i)
                if temp_date2.weekday() == day_of_week_date_obj2:
                    closest_day2 = temp_date2
                    break
                temp_date2 = prev_year_date2 - timedelta(days=i)
                if temp_date2.weekday() == day_of_week_date_obj2:
                    closest_day2 = temp_date2
                    break

            # Conversion au format français
            date_end_n_1_obj = closest_day2
            date_end_n_1_formatted = closest_day2.strftime('%d-%m-%Y')

            print("Date de début N-1:", date_start_n_1_formatted)
            print("Date de fin N-1:", date_end_n_1_formatted)

            ######################## Appel des dataframes ######################
            # Appeler la fonction pour obtenir les DataFrame nettoyés
            df = clean_adh_data()
            df_euros_players = clean_euros_players()
            df_evo_jj = clean_evo_jj()
            df_operations = load_operations()
            df_theo = clean_theo()

            ############## Vérification des dates dans les datasets ############
            # Vérifier les dates pour ADH
            errors = []

            error = check_date_range_jj(df, date_start_obj, date_end_obj, \
                date_start_n_1_obj, date_end_n_1_obj, "ADH")
            if error:
                errors.append(error)

            # Vérifier les dates pour EUROS PLAYERS
            error = check_date_range_jj(df_euros_players, date_start_obj, \
                date_end_obj, date_start_n_1_obj, date_end_n_1_obj, "PLAYERS PLUS")
            if error:
                errors.append(error)

            # Vérifier les dates pour EVO JJ
            error = check_date_range_jj(df_evo_jj, date_start_obj, \
                date_end_obj, date_start_n_1_obj, date_end_n_1_obj, "EVO JJ")
            if error:
                errors.append(error)

            # Vérifier les dates pour THEO
            error = check_date_range_jj(df_evo_jj, date_start_obj, \
                date_end_obj, date_start_n_1_obj, date_end_n_1_obj, "THEO")
            if error:
                errors.append(error)

            # Vérifier s'il y a des erreurs
            if errors:
                error_message = "<br>".join(errors)
                return render_template('analyse_jj.html', error=error_message)


            ############## ADH #################################################

            ########### Période N ###########
            # Filtrer le DataFrame en fonction des dates
            dataframe = df[(df['date'] >= date_start) & (df['date'] <= date_end)]

            # Somme du nbr d'adhésion sur la période
            result_encartage = dataframe.new_adh.sum()

            # Dataframe de répartition homme / femme
            result_genre = dataframe.groupby('Genre').agg({'new_adh': 'sum'}).reset_index()
            result_genre['proportion'] = result_genre['new_adh'] / result_genre['new_adh'].sum() * 100

            # Nbr d'ahésion par F et H
            f_value = result_genre[result_genre['Genre'] == 'F']['new_adh'].values[0]
            m_value = result_genre[result_genre['Genre'] == 'M']['new_adh'].values[0]

            # % d'ahésion par F et H
            f_prop = result_genre[result_genre['Genre'] == 'F']['proportion'].values[0]
            m_prop = result_genre[result_genre['Genre'] == 'M']['proportion'].values[0]

            ########### Période N-1 ###########
            dataframe_n_1 = df[(df['date'] >= date_start_n_1_obj) & (df['date'] <= date_end_n_1_obj)]

            # Somme du nbr d'adhésion sur la période
            result_encartage_n_1 = dataframe_n_1.new_adh.sum()

            # Dataframe de répartition homme / femme
            result_genre_n_1 = dataframe_n_1.groupby('Genre').agg({'new_adh': 'sum'}).reset_index()
            result_genre_n_1['proportion'] = result_genre_n_1['new_adh'] / result_genre_n_1['new_adh'].sum() * 100

            # Nbr d'ahésion par F et H
            # f_value_n_1 = result_genre_n_1[result_genre_n_1['Genre'] == 'F']['new_adh'].values[0]
            filtered_values = result_genre_n_1[result_genre_n_1['Genre'] == 'F']['new_adh'].values

            if filtered_values.size > 0:
                f_value_n_1 = filtered_values[0]
            else:
                f_value_n_1 = 0

            # m_value_n_1 = result_genre_n_1[result_genre_n_1['Genre'] == 'M']['new_adh'].values[0]
            filtered_values = result_genre_n_1[result_genre_n_1['Genre'] == 'M']['new_adh'].values

            if filtered_values.size > 0:
                m_value_n_1 = filtered_values[0]
            else:
                m_value_n_1 = 0

            # % d'ahésion par F et H
            # f_prop_n_1 = result_genre_n_1[result_genre_n_1['Genre'] == 'F']['proportion'].values[0]
            filtered_prop = result_genre_n_1[result_genre_n_1['Genre'] == 'F']['new_adh'].values

            if filtered_prop.size > 0:
                f_prop_n_1 = filtered_prop[0]
            else:
                f_prop_n_1 = 0

            m_prop_n_1 = result_genre_n_1[result_genre_n_1['Genre'] == 'M']['proportion'].values[0]

            ########### Evolution ###########
            evolution_encartage = ((result_encartage - result_encartage_n_1) / result_encartage_n_1) * 100




            ############## EUROS PLAYERS #######################################

            ########### Période N ###########
            # Filtrer le DataFrame en fonction des dates
            df_filtre_euros_players = df_euros_players[(df_euros_players['date'] \
                >= date_start) & (df_euros_players['date'] <= date_end)]
            df_groupby_euros_players = df_filtre_euros_players.groupby('zone')\
                .agg({'earn': 'sum', 'euros_players_rest': 'sum', 'burn': 'sum'\
                    }).reset_index()

            df_groupby_euros_players = df_groupby_euros_players.groupby('zone')\
                .agg({'earn': 'sum', 'euros_players_rest': 'sum', 'burn': 'sum'\
                    }).reset_index()
            df_groupby_euros_players[['earn', 'euros_players_rest', 'burn']] = \
                df_groupby_euros_players[['earn', 'euros_players_rest', 'burn']]\
                    .apply(lambda x: x.apply('{:,.2f}'.format).str.replace\
                        (',', ' '))

            # Afficher le total earn arrondie
            df_groupby_euros_players['earn'] = df_groupby_euros_players['earn']\
                .str.replace(' ', '').astype(float)
            total_earn = df_groupby_euros_players['earn'].sum()
            total_earn_formatted = "{:,.2f}".format(total_earn).replace(",", " ")

            # Afficher le total brun arrondie
            df_groupby_euros_players['burn'] = df_groupby_euros_players['burn']\
                .str.replace(' ', '').astype(float)
            total_burn = df_groupby_euros_players['burn'].sum()
            total_burn_formatted = "{:,.2f}".format(total_burn).replace(",", " ")

            ########### Période N-1 ###########
            # Filtrer le DataFrame en fonction des dates
            df_filtre_euros_players_n_1 = df_euros_players[(df_euros_players['date'] \
                >= date_start_n_1_obj) & (df_euros_players['date'] <= date_end_n_1_obj)]
            df_groupby_euros_players_n_1 = df_filtre_euros_players_n_1.groupby('zone')\
                .agg({'earn': 'sum', 'euros_players_rest': 'sum', 'burn': 'sum'\
                    }).reset_index()

            df_groupby_euros_players_n_1 = df_groupby_euros_players_n_1.groupby('zone')\
                .agg({'earn': 'sum', 'euros_players_rest': 'sum', 'burn': 'sum'\
                    }).reset_index()
            df_groupby_euros_players_n_1[['earn', 'euros_players_rest', 'burn']] = \
                df_groupby_euros_players_n_1[['earn', 'euros_players_rest', 'burn']]\
                    .apply(lambda x: x.apply('{:,.2f}'.format).str.replace\
                        (',', ' '))

            # Afficher le total earn arrondie
            df_groupby_euros_players_n_1['earn'] = df_groupby_euros_players_n_1['earn']\
                .str.replace(' ', '').astype(float)
            total_earn_n_1 = df_groupby_euros_players_n_1['earn'].sum()

            evolution_earn = ((total_earn - total_earn_n_1) / total_earn_n_1) * 100

            total_earn_n_1_formatted = "{:,.2f}".format(total_earn_n_1).replace(",", " ")

            # Afficher le total brun arrondie
            df_groupby_euros_players_n_1['burn'] = df_groupby_euros_players_n_1['burn']\
                .str.replace(' ', '').astype(float)
            total_burn_n_1 = df_groupby_euros_players_n_1['burn'].sum()

            evolution_burn = ((total_burn - total_burn_n_1) / total_burn_n_1) * 100

            total_burn_n_1_formatted = "{:,.2f}".format(total_burn_n_1).replace(",", " ")

            ############## CREATION DATAFRAME ##################################
            data = {
                "Infos": ["Encartages", "% de femmes", "% d'hommes", "Earn", "Burn"],
                "Période N": [result_encartage, f_prop, m_prop, total_earn_formatted, total_burn_formatted],
                "Période N-1": [result_encartage_n_1, f_prop_n_1, m_prop_n_1, total_earn_n_1_formatted, total_burn_n_1_formatted],
                "Evolution": [evolution_encartage, "", "", evolution_earn, evolution_burn],
            }

            df_euros_players = pd.DataFrame(data)



            ##################### EVO JJ #######################################

            ########### Période N ###########
            # Filtrer le DataFrame en fonction des dates
            filtre_evo_jj = df_evo_jj[(df_evo_jj['date'] >= date_start) & (df_evo_jj['date'] <= date_end)]

            # Calculer la somme de chaque colonne
            sum_freq_tot = filtre_evo_jj['freq_tot'].sum()
            sum_freq_tot_id = filtre_evo_jj['freq_tot_id'].sum()
            sum_pbj_tot = filtre_evo_jj['pbj_tot'].sum()
            sum_pbj_mas = filtre_evo_jj['pbj_mas'].sum()
            sum_pbj_jt = filtre_evo_jj['pbj_jt'].sum()

            # sum_freq_tot = "{:,.0f}".format(filtre_evo_jj['freq_tot'].sum()).replace(",", " ")
            # sum_freq_tot_id = "{:,.0f}".format(filtre_evo_jj['freq_tot_id'].sum()).replace(",", " ")
            # sum_pbj_tot = "{:,.2f}".format(filtre_evo_jj['pbj_tot'].sum()).replace(",", " ")
            # sum_pbj_mas = "{:,.2f}".format(filtre_evo_jj['pbj_mas'].sum()).replace(",", " ")
            # sum_pbj_jt = "{:,.2f}".format(filtre_evo_jj['pbj_jt'].sum()).replace(",", " ")


            ########### Période N-1 ###########
            # Filtrer le DataFrame en fonction des dates
            filtre_evo_jj_n_1 = df_evo_jj[(df_evo_jj['date'] >= date_start_n_1_obj) & (df_evo_jj['date'] <= date_end_n_1_obj)]
            # Calculer la somme de chaque colonne
            sum_freq_tot_n_1 = filtre_evo_jj_n_1['freq_tot'].sum()
            sum_freq_tot_id_n_1 = filtre_evo_jj_n_1['freq_tot_id'].sum()
            sum_pbj_tot_n_1 = filtre_evo_jj_n_1['pbj_tot'].sum()
            sum_pbj_mas_n_1 = filtre_evo_jj_n_1['pbj_mas'].sum()
            sum_pbj_jt_n_1 = filtre_evo_jj_n_1['pbj_jt'].sum()

            # sum_freq_tot_n_1 = "{:,.0f}".format(filtre_evo_jj_n_1['freq_tot'].sum()).replace(",", " ")
            # sum_freq_tot_id_n_1 = "{:,.0f}".format(filtre_evo_jj_n_1['freq_tot_id'].sum()).replace(",", " ")
            # sum_pbj_tot_n_1 = "{:,.2f}".format(filtre_evo_jj_n_1['pbj_tot'].sum()).replace(",", " ")
            # sum_pbj_mas_n_1 = "{:,.2f}".format(filtre_evo_jj_n_1['pbj_mas'].sum()).replace(",", " ")
            # sum_pbj_jt_n_1 = "{:,.2f}".format(filtre_evo_jj_n_1['pbj_jt'].sum()).replace(",", " ")


            ########### Evolution JJ ###########
            evolution_freq_tot = ((sum_freq_tot - sum_freq_tot_n_1) / sum_freq_tot_n_1) * 100
            evolution_freq_tot_id = ((sum_freq_tot_id - sum_freq_tot_id_n_1) / sum_freq_tot_id_n_1) * 100
            evolution_pbj_tot = ((sum_pbj_tot - sum_pbj_tot_n_1) / sum_pbj_tot_n_1) * 100
            evolution_pbj_mas = ((sum_pbj_mas - sum_pbj_mas_n_1) / sum_pbj_mas_n_1) * 100
            evolution_pbj_jt = ((sum_pbj_jt - sum_pbj_jt_n_1) / sum_pbj_jt_n_1) * 100




            ##################### THEO ####################################

            ########### Période N ###########
            # Filtrer le DataFrame en fonction des dates
            df_theo_filtre = df_theo[(df_theo['date'] >= date_start) & (df_theo['date'] <= date_end)]

            # Calculer la somme de chaque colonne
            total_theo = df_theo_filtre['total'].sum()

            ########### Période N-1 ###########
            df_theo_filtre_n_1 = df_theo[(df_theo['date'] >= date_start_n_1_obj) & (df_theo['date'] <= date_end_n_1_obj)]

            # Calculer la somme de chaque colonne
            total_theo_n_1 = df_theo_filtre_n_1['total'].sum()

            ########### Evolution TEHO ###########
            evolution_theo = ((total_theo - total_theo_n_1) / total_theo_n_1) * 100


            ##################### OPERATIONS ####################################

            ########### Période N ###########
            # Filtrer le DataFrame en fonction des dates
            filtre_operations = df_operations[(df_operations['DATE D'] >= date_start) & (df_operations['DATE D'] <= date_end)]
            df_operations_2 = filtre_operations[["OPE", "DATE D", "DATE F"]]

            # Formater les dates dans le format souhaité (dd-mm-yyyy)
            df_operations_2["DATE D"] = df_operations_2["DATE D"].dt.strftime('%d-%m-%Y')
            df_operations_2["DATE F"] = df_operations_2["DATE F"].dt.strftime('%d-%m-%Y')




            ############################# Return ###############################
            return render_template(
                'analyse_jj.html',
                # Dates de la période N
                date_start_obj=date_start_formatted,
                date_end_obj=date_end_formatted,
                date_start_n_1_obj=date_start_n_1_formatted,
                date_end_n_1_obj=date_end_n_1_formatted,
                # Encartage Players Plus
                result_encartage=result_encartage,
                result_encartage_n_1=result_encartage_n_1,
                evolution_encartage=evolution_encartage,
                # Répartition H / F
                f_value=f_value,
                m_value=m_value,
                f_value_n_1=f_value_n_1,
                m_value_n_1=m_value_n_1,
                f_prop=f_prop,
                m_prop=m_prop,
                f_prop_n_1=f_prop_n_1,
                m_prop_n_1=m_prop_n_1,
                # Répartition des Euros Players
                df_groupby_euros_players=df_groupby_euros_players,
                df_groupby_euros_players_n_1=df_groupby_euros_players_n_1,
                total_earn_formatted=total_earn_formatted,
                total_burn_formatted=total_burn_formatted,
                evolution_earn=evolution_earn,
                evolution_burn=evolution_burn,
                total_earn_n_1_formatted=total_earn_n_1_formatted,
                total_burn_n_1_formatted=total_burn_n_1_formatted,
                df_euros_players=df_euros_players,
                # Evo JJ Période N
                sum_freq_tot=sum_freq_tot,
                sum_freq_tot_id=sum_freq_tot_id,
                sum_pbj_tot=sum_pbj_tot,
                sum_pbj_mas=sum_pbj_mas,
                sum_pbj_jt=sum_pbj_jt,
                # Evo JJ Période N
                sum_freq_tot_n_1=sum_freq_tot_n_1,
                sum_freq_tot_id_n_1=sum_freq_tot_id_n_1,
                sum_pbj_tot_n_1=sum_pbj_tot_n_1,
                sum_pbj_mas_n_1=sum_pbj_mas_n_1,
                sum_pbj_jt_n_1=sum_pbj_jt_n_1,
                # Evolution JJ
                evolution_freq_tot=evolution_freq_tot,
                evolution_freq_tot_id=evolution_freq_tot_id,
                evolution_pbj_tot=evolution_pbj_tot,
                evolution_pbj_mas=evolution_pbj_mas,
                evolution_pbj_jt=evolution_pbj_jt,
                # THEO
                total_theo=total_theo,
                total_theo_n_1=total_theo_n_1,
                evolution_theo=evolution_theo,
                # Opérations
                df_operations_2=df_operations_2

            )

        else:
            return render_template('analyse_jj.html', error="Veuillez sélectionner des dates de début et de fin pour la période.")

    return render_template('analyse_jj.html')
