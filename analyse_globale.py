
import pandas as pd
from datetime import datetime
from flask import render_template, request
from Data_cleaning.clean_adh import clean_adh_data
from Data_cleaning.clean_euros_players import clean_euros_players
from Data_cleaning.clean_evo_jj import clean_evo_jj


def check_date_range(df, date_start_obj, date_end_obj, date_start_n_1_obj, date_end_n_1_obj, dataframe_name):
    min_date = df['date'].min()
    if date_start_obj < min_date:
        return f"Nous ne disposons que des données jusqu'au {min_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    max_date = df['date'].max()
    if date_end_obj > max_date:
        return f"Nous ne disposons que des données jusqu'au {max_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    min_date_n_1 = df['date'].min()
    if date_start_n_1_obj < min_date_n_1:
        return f"Nous ne disposons que des données jusqu'au {min_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    max_date_n_1 = df['date'].max()
    if date_end_n_1_obj > max_date_n_1:
        return f"Nous ne disposons que des données jusqu'au {max_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    return None



# @app.route('/analyse-globale', methods=['GET', 'POST'])
def analyse_globale_view():
    date_start = date_end = date_start_n_1 = date_end_n_1 = None
    dataframe = pd.DataFrame()
    df_filtre_euros_players = pd.DataFrame()

    if request.method == 'POST':
        date_start = request.form.get('date-depart')
        date_end = request.form.get('date-arrivee')
        date_start_n_1 = request.form.get('date-depart-n-1')
        date_end_n_1 = request.form.get('date-arrivee-n-1')

        ####################### Dates de la période N ##########################
        # Vérifier que date_start et date_end ne sont pas vides
        if date_start and date_end and date_start_n_1 and date_end_n_1:
            # Convertir les chaînes de caractères en objets de date
            date_start_obj = datetime.strptime(date_start, '%Y-%m-%d')
            date_end_obj = datetime.strptime(date_end, '%Y-%m-%d')
            date_start_n_1_obj = datetime.strptime(date_start_n_1, '%Y-%m-%d')
            date_end_n_1_obj = datetime.strptime(date_end_n_1, '%Y-%m-%d')

            # Formater les dates dans le format souhaité (dd-mm-yyyy)
            date_start_formatted = date_start_obj.strftime('%d-%m-%Y')
            date_end_formatted = date_end_obj.strftime('%d-%m-%Y')
            date_start_n_1_formatted = date_start_n_1_obj.strftime('%d-%m-%Y')
            date_end_n_1_formatted = date_end_n_1_obj.strftime('%d-%m-%Y')

            ######################## Appel des dataframes ######################
            # Appeler la fonction pour obtenir les DataFrame nettoyés
            df = clean_adh_data()
            df_euros_players = clean_euros_players()
            # df_evo_jj = clean_evo_jj()

            ############## Vérification des dates dans les datasets ############
            # Vérifier les dates pour ADH
            errors = []

            error = check_date_range(df, date_start_obj, date_end_obj, \
                date_start_n_1_obj, date_end_n_1_obj, "ADH")
            if error:
                errors.append(error)

            # Vérifier les dates pour EUROS PLAYERS
            error = check_date_range(df_euros_players, date_start_obj, \
                date_end_obj, date_start_n_1_obj, date_end_n_1_obj, "PLAYERS PLUS")
            if error:
                errors.append(error)

            # Vérifier les dates pour EVO JJ
            # error = check_date_range(df_evo_jj, date_start_obj, date_end_obj, "EVO JJ")
            # if error:
            #     errors.append(error)

            # Vérifier s'il y a des erreurs
            if errors:
                error_message = "<br>".join(errors)
                return render_template('analyse_globale.html', error=error_message)


            ############## ADH #################################################

            ########### Période N ###########
            # Filtrer le DataFrame en fonction des dates
            dataframe = df[(df['date'] >= date_start) & (df['date'] <= date_end)]

            # Somme du nbr d'adhésion sur la période
            result_encartage = dataframe.new_adh.sum()

            # Dataframe de répartition homme / femme
            result_genre = dataframe.groupby('Genre').agg({'new_adh': 'sum'}).reset_index()
            result_genre['proportion'] = result_genre['new_adh'] / result_genre['new_adh'].sum() * 100

            # Nbr d'ahésion par F et H
            f_value = result_genre[result_genre['Genre'] == 'F']['new_adh'].values[0]
            m_value = result_genre[result_genre['Genre'] == 'M']['new_adh'].values[0]

            # % d'ahésion par F et H
            f_prop = result_genre[result_genre['Genre'] == 'F']['proportion'].values[0]
            m_prop = result_genre[result_genre['Genre'] == 'M']['proportion'].values[0]

            ########### Période N-1 ###########
            dataframe_n_1 = df[(df['date'] >= date_start_n_1) & (df['date'] <= date_end_n_1)]

            # Somme du nbr d'adhésion sur la période
            result_encartage_n_1 = dataframe_n_1.new_adh.sum()

            # Dataframe de répartition homme / femme
            result_genre_n_1 = dataframe_n_1.groupby('Genre').agg({'new_adh': 'sum'}).reset_index()
            result_genre_n_1['proportion'] = result_genre_n_1['new_adh'] / result_genre_n_1['new_adh'].sum() * 100

            # Nbr d'ahésion par F et H
            f_value_n_1 = result_genre_n_1[result_genre_n_1['Genre'] == 'F']['new_adh'].values[0]
            m_value_n_1 = result_genre_n_1[result_genre_n_1['Genre'] == 'M']['new_adh'].values[0]

            # % d'ahésion par F et H
            f_prop_n_1 = result_genre_n_1[result_genre_n_1['Genre'] == 'F']['proportion'].values[0]
            m_prop_n_1 = result_genre_n_1[result_genre_n_1['Genre'] == 'M']['proportion'].values[0]

            ########### Evolution ###########
            evolution_encartage = ((result_encartage - result_encartage_n_1) / result_encartage_n_1) * 100




            ############## EUROS PLAYERS #######################################

            ########### Période N ###########
            # Filtrer le DataFrame en fonction des dates
            df_filtre_euros_players = df_euros_players[(df_euros_players['date'] \
                >= date_start) & (df_euros_players['date'] <= date_end)]
            df_groupby_euros_players = df_filtre_euros_players.groupby('zone')\
                .agg({'earn': 'sum', 'euros_players_rest': 'sum', 'burn': 'sum'\
                    }).reset_index()

            df_groupby_euros_players = df_groupby_euros_players.groupby('zone')\
                .agg({'earn': 'sum', 'euros_players_rest': 'sum', 'burn': 'sum'\
                    }).reset_index()
            df_groupby_euros_players[['earn', 'euros_players_rest', 'burn']] = \
                df_groupby_euros_players[['earn', 'euros_players_rest', 'burn']]\
                    .apply(lambda x: x.apply('{:,.2f}'.format).str.replace\
                        (',', ' '))

            # Afficher le total earn arrondie
            df_groupby_euros_players['earn'] = df_groupby_euros_players['earn']\
                .str.replace(' ', '').astype(float)
            total_earn = df_groupby_euros_players['earn'].sum()
            total_earn_formatted = "{:,.2f}".format(total_earn).replace(",", " ")

            # Afficher le total brun arrondie
            df_groupby_euros_players['burn'] = df_groupby_euros_players['burn']\
                .str.replace(' ', '').astype(float)
            total_burn = df_groupby_euros_players['burn'].sum()
            total_burn_formatted = "{:,.2f}".format(total_burn).replace(",", " ")

            ########### Période N-1 ###########
            # Filtrer le DataFrame en fonction des dates
            df_filtre_euros_players_n_1 = df_euros_players[(df_euros_players['date'] \
                >= date_start_n_1) & (df_euros_players['date'] <= date_end_n_1)]
            df_groupby_euros_players_n_1 = df_filtre_euros_players_n_1.groupby('zone')\
                .agg({'earn': 'sum', 'euros_players_rest': 'sum', 'burn': 'sum'\
                    }).reset_index()

            df_groupby_euros_players_n_1 = df_groupby_euros_players_n_1.groupby('zone')\
                .agg({'earn': 'sum', 'euros_players_rest': 'sum', 'burn': 'sum'\
                    }).reset_index()
            df_groupby_euros_players_n_1[['earn', 'euros_players_rest', 'burn']] = \
                df_groupby_euros_players_n_1[['earn', 'euros_players_rest', 'burn']]\
                    .apply(lambda x: x.apply('{:,.2f}'.format).str.replace\
                        (',', ' '))

            # Afficher le total earn arrondie
            df_groupby_euros_players_n_1['earn'] = df_groupby_euros_players_n_1['earn']\
                .str.replace(' ', '').astype(float)
            total_earn_n_1 = df_groupby_euros_players_n_1['earn'].sum()
            total_earn_n_1_formatted = "{:,.2f}".format(total_earn_n_1).replace(",", " ")

            # Afficher le total brun arrondie
            df_groupby_euros_players_n_1['burn'] = df_groupby_euros_players_n_1['burn']\
                .str.replace(' ', '').astype(float)
            total_burn_n_1 = df_groupby_euros_players_n_1['burn'].sum()
            total_burn_n_1_formatted = "{:,.2f}".format(total_burn_n_1).replace(",", " ")


            ############## EVO JJ ##############################################


            ############################# Return ###############################
            return render_template(
                'analyse_globale.html',
                # Dates de la période N
                date_start_obj=date_start_formatted,
                date_end_obj=date_end_formatted,
                date_start_n_1_obj=date_start_n_1_formatted,
                date_end_n_1_obj=date_end_n_1_formatted,
                # Encartage Players Plus
                result_encartage=result_encartage,
                result_encartage_n_1=result_encartage_n_1,
                evolution_encartage=evolution_encartage,
                # Répartition H / F
                f_value=f_value,
                m_value=m_value,
                f_value_n_1=f_value_n_1,
                m_value_n_1=m_value_n_1,
                f_prop=f_prop,
                m_prop=m_prop,
                f_prop_n_1=f_prop_n_1,
                m_prop_n_1=m_prop_n_1,
                # Répartition des Euros Players
                df_groupby_euros_players=df_groupby_euros_players,
                df_groupby_euros_players_n_1=df_groupby_euros_players_n_1,
                total_earn_formatted=total_earn_formatted,
                total_burn_formatted=total_burn_formatted,
                total_earn_n_1_formatted=total_earn_n_1_formatted,
                total_burn_n_1_formatted=total_burn_n_1_formatted
            )

        else:
            return render_template('analyse_globale.html', error="Veuillez sélectionner des dates de début et de fin pour les 2 périodes à comparer.")

    return render_template('analyse_globale.html')
