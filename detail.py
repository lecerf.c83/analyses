
import pandas as pd
from datetime import datetime, timedelta
from flask import render_template, request

from Data_cleaning.clean_evo_jj import clean_evo_jj
import ipywidgets as widgets
from IPython.display import display
import matplotlib.pyplot as plt



def is_number(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def check_date_range_jj(df, date_start_obj, date_end_obj, date_start_n_1_obj, date_end_n_1_obj, dataframe_name):
    min_date = df['date'].min()
    if date_start_obj < min_date:
        return f"Nous ne disposons des données qu'à partir du {min_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    max_date = df['date'].max()
    if date_end_obj > max_date:
        return f"Nous ne disposons que des données jusqu'au {max_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    min_date_n_1 = df['date'].min()
    if date_start_n_1_obj < min_date_n_1:
        return f"Nous ne disposons des données qu'à partir du {min_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    max_date_n_1 = df['date'].max()
    if date_end_n_1_obj > max_date_n_1:
        return f"Nous ne disposons que des données jusqu'au {max_date.strftime('%d-%m-%Y')} pour {dataframe_name}."

    return None



# @app.route('/analyse-jj', methods=['GET', 'POST'])
def detail_view():
    date_start = date_end = None
    filtre_evo_jj = pd.DataFrame()

    if request.method == 'POST':
        date_start = request.form.get('date-depart')
        date_end = request.form.get('date-arrivee')
        year_comparaison = request.form.get('year-comparaison')

        # Convertir l'année de comparaison en entier, ou utiliser une valeur par défaut si elle est vide
        if year_comparaison:
            year_comparaison = int(year_comparaison)
        else:
            year_comparaison = datetime.now().year - 1

        ####################### Vérifier dates  ################################
        # Vérifier que date_start et date_end ne sont pas vides
        if date_start and date_end and year_comparaison :
            ############################# Période N ############################
            # Convertir les chaînes de caractères en objets de date
            date_start_obj = datetime.strptime(date_start, '%Y-%m-%d')
            date_end_obj = datetime.strptime(date_end, '%Y-%m-%d')

            # Formater les dates dans le format souhaité (dd-mm-yyyy)
            date_start_formatted = date_start_obj.strftime('%d-%m-%Y')
            date_end_formatted = date_end_obj.strftime('%d-%m-%Y')

            print("Date de début N:", date_start_formatted)
            print("Date de fin N:", date_end_formatted)

            #################### Période N-1 date de départ ####################
            # Vérifiez si l'année précédente est une année bissextile
            def is_leap_year(year):
                return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)

            # Aller à la même date de l'année précédente, mais si le jour est \
                # 29 et que l'année précédente n'est pas bissextile, définir le \
                    # jour à 28
            prev_year_date = date_start_obj.replace(day=28) if date_start_obj.day \
                == 29 and not is_leap_year(date_start_obj.year - 1) else date_start_obj
            # prev_year_date = prev_year_date.replace(year = date_start_obj.year - 1)
            prev_year_date = prev_year_date.replace(year = year_comparaison)

            # Vérifiez le jour de la semaine pour date_start_obj et prev_year_date
            day_of_week_date_obj = date_start_obj.weekday()
            day_of_week_prev_year = prev_year_date.weekday()

            # Trouver le jour de la semaine le plus proche dans l'année précédente
            for i in range(7):
                temp_date = prev_year_date + timedelta(days=i)
                if temp_date.weekday() == day_of_week_date_obj:
                    closest_day = temp_date
                    break
                temp_date = prev_year_date - timedelta(days=i)
                if temp_date.weekday() == day_of_week_date_obj:
                    closest_day = temp_date
                    break

            # Conversion au format français
            date_start_n_1_obj = closest_day
            date_start_n_1_formatted = closest_day.strftime('%d-%m-%Y')

            #################### Période N-1 date de fin ####################
            # Vérifiez si l'année précédente est une année bissextile
            def is_leap_year(year):
                return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)

            # Aller à la même date de l'année précédente, mais si le jour est \
                # 29 et que l'année précédente n'est pas bissextile, définir le \
                    # jour à 28
            prev_year_date2 = date_end_obj.replace(day=28) if date_end_obj.day \
                == 29 and not is_leap_year(date_end_obj.year - 1) else date_end_obj
            # prev_year_date2 = prev_year_date2.replace(year = date_end_obj.year - 1)
            prev_year_date2 = prev_year_date2.replace(year=year_comparaison)


            # Vérifiez le jour de la semaine
            day_of_week_date_obj2 = date_end_obj.weekday()
            # day_of_week_prev_year2 = prev_year_date2.weekday()

            # Trouver le jour de la semaine le plus proche dans l'année précédente
            for i in range(7):
                temp_date2 = prev_year_date2 + timedelta(days=i)
                if temp_date2.weekday() == day_of_week_date_obj2:
                    closest_day2 = temp_date2
                    break
                temp_date2 = prev_year_date2 - timedelta(days=i)
                if temp_date2.weekday() == day_of_week_date_obj2:
                    closest_day2 = temp_date2
                    break

            # Conversion au format français
            date_end_n_1_obj = closest_day2
            date_end_n_1_formatted = closest_day2.strftime('%d-%m-%Y')

            print("Date de début N-1:", date_start_n_1_formatted)
            print("Date de fin N-1:", date_end_n_1_formatted)

            ######################## Appel des dataframes ######################
            # Appeler la fonction pour obtenir les DataFrame nettoyés

            df_evo_jj = clean_evo_jj()

            ############## Vérification des dates dans les datasets ############
            # Vérifier les dates pour ADH
            errors = []



            # Vérifier les dates pour EVO JJ
            error = check_date_range_jj(df_evo_jj, date_start_obj, \
                date_end_obj, date_start_n_1_obj, date_end_n_1_obj, "EVO JJ")
            if error:
                errors.append(error)


            # Vérifier s'il y a des erreurs
            if errors:
                error_message = "<br>".join(errors)
                return render_template('analyse_jj.html', error=error_message)




            ##################### EVO JJ #######################################

            ########### Période N ###########
            # Filtrer le DataFrame en fonction des dates
            filtre_evo_jj = df_evo_jj[(df_evo_jj['date'] >= date_start) & (df_evo_jj['date'] <= date_end)]

            df_widget = filtre_evo_jj.copy()  # Création d'une copie du dataframe

            # Convertir la colonne 'date' en datetime
            df_widget['date'] = pd.to_datetime(df_widget['date'])

            # Créer les widgets
            group_by_widget = widgets.Dropdown(options=['Jour', 'Mois et Jour'], description='Trier par:')
            data_type_widget = widgets.Dropdown(options=['freq_tot', 'freq_tot_id', 'pbj_tot', 'pbj_mas', 'pbj_jt'], description='Type de données:')
            month_widget = widgets.Dropdown(description='Mois:')

            # Fonction pour mettre à jour les options du widget de mois
            def update_month_options(*args):
                if group_by_widget.value == 'Mois et Jour':
                    df_widget['mois'] = df_widget['date'].dt.to_period("M")
                    unique_months = df_widget['mois'].unique().astype(str).tolist()
                    month_widget.options = unique_months
                    month_widget.value = unique_months[0]  # affiche par défaut le 1er mois présent dans le dataset
                    month_widget.layout.visibility = 'visible'
                else:
                    month_widget.layout.visibility = 'hidden'

            group_by_widget.observe(update_month_options, 'value')

            # Fonction pour afficher le graphique
            def show_grouped_data(group_by, data_type, month):
                days_order = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']

                if group_by == 'Jour':
                    df_widget['jour'] = pd.Categorical(df_widget['jour'], categories=days_order, ordered=True)
                    grouped = df_widget.groupby('jour')[data_type].sum()

                elif group_by == 'Mois et Jour' and month:
                    df_widget['mois'] = df_widget['date'].dt.to_period("M")
                    df_widget['jour'] = pd.Categorical(df_widget['jour'], categories=days_order, ordered=True)
                    grouped = df_widget[df_widget['mois'].astype(str) == month].groupby(['mois', 'jour'])[data_type].sum()

                grouped.plot(kind='bar')
                plt.ylabel(data_type)
                plt.show()

            # Afficher les widgets
            display(group_by_widget, data_type_widget, month_widget)

            # Afficher le graphique en fonction des widgets
            output = widgets.interactive_output(show_grouped_data, {'group_by': group_by_widget, 'data_type': data_type_widget, 'month': month_widget})
            display(output)

            # Mettre à jour les options du widget de mois initialement
            update_month_options()




            ############################# Return ###############################
            return render_template(
                'detail.html',
                # Dates de la période N
                date_start_obj=date_start_formatted,
                date_end_obj=date_end_formatted,
                date_start_n_1_obj=date_start_n_1_formatted,
                date_end_n_1_obj=date_end_n_1_formatted,
                # Evo JJ Widget
                widget=widgets


            )

        else:
            return render_template('detail.html', error="Veuillez sélectionner des dates de début et de fin pour la période.")

    return render_template('detail.html')
