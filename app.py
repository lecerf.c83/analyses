from flask import Flask, render_template, request, session
from dotenv import load_dotenv
from config import Config
from export import upload_adh, upload_euros_players, upload_evo_jj, upload_marketing, upload_theo
from analyse_globale import analyse_globale_view
from analyse_jj import analyse_jj_view, is_number
from detail import detail_view
from gcp import client, bucket


load_dotenv()  # Charger les variables d'environnement à partir du fichier .env

app = Flask(__name__)
app.config.from_object(Config)

app.jinja_env.filters['is_number'] = is_number

# Root vers les autres pages
@app.route('/')
def home():
    return render_template('home.html')

@app.route('/export')
def export():
    return render_template('export.html')

@app.route('/analyse-globale', methods=['GET', 'POST'])
def analyse_globale():
    return analyse_globale_view()

@app.route('/analyse-jj', methods=['GET', 'POST'])
def analyse_jj():
    return analyse_jj_view()

@app.route('/detail', methods=['GET', 'POST'])
def detail():
    return detail_view()


######## Export vers GCP ###########

@app.route('/upload_adh', methods=['POST'])
def upload_adh_route():
    return upload_adh()

@app.route('/upload_euros_players', methods=['POST'])
def upload_euros_players_route():
    return upload_euros_players()

@app.route('/upload_evo_jj', methods=['POST'])
def upload_evo_jj_route():
    return upload_evo_jj()

@app.route('/upload_marketing', methods=['POST'])
def upload_marketing_route():
    return upload_marketing()

@app.route('/upload_theo', methods=['POST'])
def upload_theo_route():
    return upload_theo()

###################################



@app.template_filter('format_number')
def format_number(value):
    if isinstance(value, (int, float)):
        return "{:,.0f}".format(value).replace(",", " ")
    else:
        return value

@app.template_filter('format_percent')
def format_percent(value):
    return "{:.2f}".format(value)



# Main
if __name__ == '__main__':
    app.run(debug=True)
