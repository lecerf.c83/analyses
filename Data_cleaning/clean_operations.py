import pandas as pd
# from google.cloud import storage
# from google.oauth2 import service_account
# import os
from dotenv import load_dotenv
from gcp import client, bucket


load_dotenv() # Charger les variables d'environnement à partir du fichier .env


# Lister les fichiers du bucket
# blobs = bucket.list_blobs()

# for blob in blobs:
#     print(blob.name)




def load_operations():
    # Chemin vers le fichier OPERATIONS.xls dans le bucket
    blob_path = "MARKETING/OPERATIONS.xls"

    # Récupérer le blob spécifique
    blob = bucket.blob(blob_path)

    # Télécharger le contenu du blob en tant qu'objets bytes
    blob_data = blob.download_as_bytes()

    # Vérifier si le fichier est vide avant de le lire
    if len(blob_data) > 0:
        load_df = pd.read_excel(blob_data)
    else:
        load_df = pd.DataFrame()  # Créer un dataframe vide si le fichier est vide

    return load_df



def clean_operations():

    # Appeler la fonction pour concaténer les données ADH
    df = load_operations()

    # Supprimer les lignes en doublon
    df = df.drop_duplicates()




    return df







if __name__ == '__main__':
    # Appeler la fonction pour concaténer les données ADH
    print(clean_operations().info())
