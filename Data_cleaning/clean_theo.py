import pandas as pd
from dotenv import load_dotenv
from gcp import client, bucket
import os


load_dotenv() # Charger les variables d'environnement à partir du fichier .env

def concatenate_theo():
    # Chemin vers le dossier contenant les fichiers XLSX dans le bucket
    blob_directory = "THEO/"

    # Liste des fichiers XLSX dans le répertoire spécifié
    blobs = bucket.list_blobs(prefix=blob_directory)

    # Initialisation du dataframe pour stocker les données concaténées
    concatenated_df = pd.DataFrame()

    # Boucle pour lire chaque fichier XLS et les concaténer
    for blob in blobs:
        # Télécharger le contenu du blob en tant qu'objets bytes
        blob_data = blob.download_as_bytes()

        # Vérifier si le fichier est vide avant de le lire
        if len(blob_data) > 0:
            df = pd.read_excel(blob_data)

            # Concaténation avec le dataframe principal
            concatenated_df = pd.concat([concatenated_df, df], ignore_index=True)

    return concatenated_df


def clean_theo():

    # Appeler la fonction pour concaténer les données ADH
    df = concatenate_theo()

    # Supprimer les lignes en doublon
    df = df.drop_duplicates()

    # Suppression des lignes avec une date vide
    df.dropna(subset=['date'], inplace=True)

    # Fonction pour obtenir le jour de la semaine en français
    def get_day_of_week_in_french(date_obj):
        # Obtenir le jour de la semaine (0 = Lundi, 1 = Mardi, ..., 6 = Dimanche)
        day_of_week = date_obj.weekday()
        # Liste des jours de la semaine en français
        days_in_french = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
        # Retourner le jour de la semaine en français
        return days_in_french[day_of_week]

    # Ajouter une nouvelle colonne 'jour' au DataFrame
    df['jour'] = df['date'].apply(get_day_of_week_in_french)


    return df





if __name__ == '__main__':
    # Appeler la fonction pour concaténer les données ADH
    print(clean_theo().info())
