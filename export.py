from flask import request, redirect, url_for, render_template, flash
import pandas as pd
import io
from gcp import client, bucket


def upload_file(blob_path, error_form, group_by_columns=None, agg_dict=None):
    if 'excel_file' not in request.files:
        return flash('Aucun fichier n\'a été fourni.', 'error')


    excel_file = request.files['excel_file']

    if not excel_file.filename.endswith('.xls'):
        return flash('Le fichier doit être un fichier .xls', 'error')


    # Sauvegarder le fichier dans ton bucket
    blob = bucket.blob(blob_path + excel_file.filename)

    # Uniquement pour le fichier Euros Players
    if group_by_columns and agg_dict:
        try:
            df = pd.read_excel(excel_file)
        except Exception as e:
            return flash(f'Erreur lors de l\'ouverture du fichier Excel : {str(e)}', 'error')

        df = df.groupby(group_by_columns).agg(agg_dict).reset_index()

        output = io.BytesIO()
        with pd.ExcelWriter(output, engine='xlsxwriter') as writer:
            df.to_excel(writer, index=False)

        output.seek(0)
        try:
            blob.upload_from_file(output, content_type='application/vnd.ms-excel')
        except Exception as e:
            return flash(f'Erreur lors de l\'upload sur le bucket : {str(e)}', 'error')

    else:
        # Pour les autres fichiers
        try:
            blob.upload_from_file(excel_file)
        except Exception as e:
            return flash(f'Erreur lors de l\'upload sur le bucket : {str(e)}', 'error')

    flash('Le fichier a été envoyé', 'success')
    return redirect(url_for('export'))


def upload_adh():
    upload_file('ADH/', 'adh')
    return redirect(url_for('export'))


def upload_euros_players():
    group_by_columns = ['Date exploitation', 'Zone']
    agg_dict = {'euros Players gagnés': sum, 'euros Players restants': sum}
    upload_file('EUROS_PLAYERS/', 'euros_players', group_by_columns, agg_dict)
    return redirect(url_for('export'))


def upload_evo_jj():
    upload_file('EVO_JJ/', 'evo_jj')
    return redirect(url_for('export'))

def upload_marketing():
    upload_file('MARKETING/', 'marketing')
    return redirect(url_for('export'))

def upload_theo():
    upload_file('THEO/', 'theo')
    return redirect(url_for('export'))
